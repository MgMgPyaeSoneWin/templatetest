<!DOCTYPE html>
<html lang='en'>
  <head>

    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>CV Builder Registration</title>
    <?php require_once '../worker_header.php'; ?>
  </head>
<body class="skin-teal sidebar-mini">
  <div>
    <div class="wrapper">

      <?php require_once '../worker_navbar.php'; ?>
      <?php require_once '../worker_sidebar.php'; ?>

      <!-- Content Wrapper. Contains page content -->
        <section class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Search Jobs
                    <small>Version 2.0</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li class="active">Search Jobs</li>
                </ol>
            </section>

            <!-- Main content -->
            <section class="content">
                <!-- Info boxes -->
                <div class="row">
                    <div class="col-md-12 col-sm-6 col-xs-12">
                        <div class="card cardPadding">
                            <div class="cardContent">
                                <div class="cardheader">
                                    <h4 class="h4-font-size">Search Jobs
                                    </h4>
                                </div>
                                <div class="input-group input-group-lg">
                                    <input type="text" class="form-control">
                                    <span class="input-group-btn">
                                        <button class="btn btn-info btn-flat" type="button">Go!</button>
                                    </span>
                                </div>

                                <!-- checkbox -->
                                <div class="form-group">
                                    <label>
                                        <input type="checkbox" class="flat-red" checked/>
                                        PHP
                                    </label>
                                    <label>
                                        <input type="checkbox" class="flat-red"/>
                                        Java
                                    </label>
                                    <label>
                                        <input type="checkbox" class="flat-red" disabled/>
                                        Android
                                    </label>
                                    <label>
                                        <input type="checkbox" class="flat-red" disabled/>
                                        iOS
                                    </label>
                                </div>

                                <nav aria-label="Page navigation" class="pull-right">
                                    <ul class="pagination">
                                        <li>
                                            <a href="#" aria-label="Previous">
                                                <span aria-hidden="true">&laquo;</span>
                                            </a>
                                        </li>
                                        <li><a href="#">1</a></li>
                                        <li><a href="#">2</a></li>
                                        <li><a href="#">3</a></li>
                                        <li><a href="#">4</a></li>
                                        <li><a href="#">5</a></li>
                                        <li>
                                            <a href="#" aria-label="Next">
                                                <span aria-hidden="true">&raquo;</span>
                                            </a>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div><!-- /.col -->
                </div>
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <a href="postedJobsDetail.html">
                            <div class="card cardPadding">
                                <div class="cardContent">
                                    <div class="cardheader">
                                        <h4 class="h4-font-size">
                                           New West Minster Instute
                                        </h4>
                                    </div>
                                    <p class="disableText">
                                        By Admin / May 3rd,2014 / Design / 1 comment
                                    </p>
                                    <dl class="dl-horizontal">
                                        <dt>Degree</dt>
                                        <dd>A description list is perfect for defining terms.</dd>
                                        <dt>Description</dt>
                                        <dd>Vestibulum id ligula porta felis euismod semper eget lacinia odio sem nec
                                            elit.
                                        </dd>
                                        <dd>Donec id elit non mi porta gravida at eget metus.</dd>
                                        <dt>Malesuada porta</dt>
                                        <dd>Etiam porta sem malesuada magna mollis euismod.</dd>
                                        <dt>Felis euismod semper eget lacinia</dt>
                                        <dd>Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut
                                            fermentum massa justo sit amet risus.
                                        </dd>
                                    </dl>
                                    <p class="left-and-right-algin">
                                        <i class="fa fa-heart fa-heart-o custom-heart"></i><i class="right-align">See
                                        more</i>
                                    </p>
                                </div>
                            </div>
                        </a>
                    </div><!-- /.col -->
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="card cardPadding">
                            <div class="cardContent">
                                <div class="cardheader">
                                    <h4 class="h4-font-size">
                                       New West Minster Instute
                                    </h4>
                                </div>
                                <p class="disableText">
                                    By Admin / May 3rd,2014 / Design / 1 comment
                                </p>
                                <dl class="dl-horizontal">
                                    <dt>Degree</dt>
                                    <dd>A description list is perfect for defining terms.</dd>
                                    <dt>Description</dt>
                                    <dd>Vestibulum id ligula porta felis euismod semper eget lacinia odio sem nec
                                        elit.
                                    </dd>
                                    <dd>Donec id elit non mi porta gravida at eget metus.</dd>
                                    <dt>Malesuada porta</dt>
                                    <dd>Etiam porta sem malesuada magna mollis euismod.</dd>
                                    <dt>Felis euismod semper eget lacinia</dt>
                                    <dd>Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut
                                        fermentum massa justo sit amet risus.
                                    </dd>
                                </dl>
                                <p class="left-and-right-algin">
                                    <i class="fa fa-heart fa-heart-o custom-heart"></i><i class="right-align">See
                                    more</i>
                                </p>
                            </div>
                        </div>
                    </div><!-- /.col -->
                </div>
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="card cardPadding">
                            <div class="cardContent">
                                <div class="cardheader">
                                    <h4 class="h4-font-size">
                                       New West Minster Instute
                                    </h4>
                                </div>
                                <p class="disableText">
                                    By Admin / May 3rd,2014 / Design / 1 comment
                                </p>
                                <dl class="dl-horizontal">
                                    <dt>Degree</dt>
                                    <dd>A description list is perfect for defining terms.</dd>
                                    <dt>Description</dt>
                                    <dd>Vestibulum id ligula porta felis euismod semper eget lacinia odio sem nec
                                        elit.
                                    </dd>
                                    <dd>Donec id elit non mi porta gravida at eget metus.</dd>
                                    <dt>Malesuada porta</dt>
                                    <dd>Etiam porta sem malesuada magna mollis euismod.</dd>
                                    <dt>Felis euismod semper eget lacinia</dt>
                                    <dd>Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut
                                        fermentum massa justo sit amet risus.
                                    </dd>
                                </dl>
                                <p class="left-and-right-algin">
                                    <i class="fa fa-heart fa-heart-o custom-heart"></i><i class="right-align">See
                                    more</i>
                                </p>
                            </div>
                        </div>
                    </div><!-- /.col -->
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="card cardPadding">
                            <div class="cardContent">
                                <div class="cardheader">
                                    <h4 class="h4-font-size">
                                        New West Minster Institute
                                    </h4>
                                </div>
                                <p class="disableText">
                                    By Admin / May 3rd,2014 / Design / 1 comment
                                </p>
                                <dl class="dl-horizontal">
                                    <dt>Degree</dt>
                                    <dd>A description list is perfect for defining terms.</dd>
                                    <dt>Description</dt>
                                    <dd>Vestibulum id ligula porta felis euismod semper eget lacinia odio sem nec
                                        elit.
                                    </dd>
                                    <dd>Donec id elit non mi porta gravida at eget metus.</dd>
                                    <dt>Malesuada porta</dt>
                                    <dd>Etiam porta sem malesuada magna mollis euismod.</dd>
                                    <dt>Felis euismod semper eget lacinia</dt>
                                    <dd>Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut
                                        fermentum massa justo sit amet risus.
                                    </dd>
                                </dl>
                                <p class="left-and-right-algin">
                                    <i class="fa fa-heart fa-heart-o custom-heart"></i><i class="right-align">See
                                    more</i>
                                </p>
                            </div>
                        </div>
                    </div><!-- /.col -->
                </div>
                <!-- fix for small devices only -->
                <div class="clearfix visible-sm-block"></div>

            </section><!-- /.row -->

            <div class="row">
                <div class="col-md-12">


                </div><!-- /.col -->
            </div><!-- /.row -->


        </section><!-- /.content -->


      <?php require_once '../worker_footer.php'; ?>
  </div>
</body>
</html>
