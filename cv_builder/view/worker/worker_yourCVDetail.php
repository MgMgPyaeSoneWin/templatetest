<!DOCTYPE html>
<html lang='en'>
  <head>

    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>CV Builder Registration</title>
    <?php require_once '../worker_header.php'; ?>
  </head>
<body class="skin-teal sidebar-mini">
  <div>
    <div class="wrapper">

      <?php require_once '../worker_navbar.php'; ?>
      <?php require_once '../worker_sidebar.php'; ?>

      <section class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    CV Details
                    <small>Version 2.0</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li class="active">CV Detail</li>
                </ol>
            </section>

            <!-- Main content -->
            <section class="content">
                <!-- Info boxes -->
                <div class="row">
                    <div class="col-md-12 col-sm-6 col-xs-12">
                        <div class="card cardPadding">
                            <div class="cardContent">
                                <div class="cardheader">
                                    <h3 class="h4-font-size">
                                        Contact Information
                                    </h3>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 col-md-offset-1">
                                        <img src="../../img/user2-160x160.jpg" class="img-rounded" style="border: 3px solid;
    border-color: #757575;">
                                    </div>
                                    <div class="col-md-8">
                                        <h1>John Smith</h1>
                                        <h4>Font-End Developer</h4>

                                    </div>
                                </div>

                                <hr class="mini-line">

                                <div class="row">
                                    <div class="col-md-3 col-md-offset-1">
                                        <h4><b>Personal Info</b></h4>
                                    </div>
                                    <div class="col-md-8">
                                        <p>Address: No.38, Padather Street, Maynigone, SanChaung, Yangon</p>
                                        <p>Email: mgmgpyaesonewin@gmail.com</p>
                                        <p>Website: www.johnsmith.com</p>
                                        <p>Phone: 09796874359</p>
                                    </div>
                                </div>

                                <hr class="mini-line">

                                <div class="row">
                                    <div class="col-md-3 col-md-offset-1">
                                        <h4><b>Summary</b></h4>
                                    </div>
                                    <div class="col-md-8">
                                        <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur
                                            ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium
                                            quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel,
                                            aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a,
                                            venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer
                                            tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate
                                            eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend
                                            ac, enim.</p>
                                    </div>
                                </div>

                                <hr class="mini-line">

                                <div class="row">
                                    <div class="col-md-3 col-md-offset-1">
                                        <h4><b>Project</b></h4>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="row">
                                            <div class="col-lg-8">
                                                <h4 class="blue-text"><b>One Short CV Project</b></h4>
                                                <p>Aenean leo ligula, porttitor</p>

                                                <ul>
                                                    <li>Cras dapibus dapibus</li>
                                                    <li>Vivamus elementum semper nisi</li>
                                                    <li>In enim justo, rhoncus ut</li>
                                                    <!--<li>Used Technologies:-->
                                                        <!--<div class="tag">PHP</div>-->
                                                        <!--<div class="tag">Android</div>-->
                                                        <!--<div class="tag">Javascript</div>-->
                                                        <!--<div class="tag">CSS</div>-->
                                                    <!--</li>-->

                                                </ul>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="timesteamp-tag">10-6-2014 to Present</div>
                                            </div>
                                        </div>

                                        <hr class="mini-line">

                                        <div class="row">
                                            <div class="col-lg-8">
                                                <h4 class="blue-text"><b>Kantgu Project</b></h4>
                                                <p>Cum sociis natoque penatibus</p>

                                                <ul>
                                                    <li>Vivamus elementum semper nisi</li>
                                                    <li>Cras dapibus dapibus</li>
                                                    <li>Cras dapibus dapibus</li>
                                                    <!--<li>Used Technologies:-->
                                                        <!--<div class="tag">PHP</div>-->
                                                        <!--<div class="tag">Android</div>-->
                                                        <!--<div class="tag">Javascript</div>-->
                                                        <!--<div class="tag">CSS</div>-->
                                                    <!--</li>-->
                                                </ul>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="timesteamp-tag">10-6-2014 to 14-3-2015</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <hr class="mini-line">

                                <div class="row">
                                    <div class="col-md-3 col-md-offset-1">
                                        <h4><b>Technical Skills</b></h4>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="tag">PHP</div>
                                        <div class="tag">Android</div>
                                        <div class="tag">Javascript</div>
                                        <div class="tag">CSS</div>
                                    </div>
                                </div>

                                <hr class="mini-line">
                                <div class="row">
                                    <div class="col-md-3 col-md-offset-1">
                                        <h4><b>Education</b></h4>
                                    </div>
                                    <div class="col-md-8">
                                        <h4 class="blue-text"><b>B.C Sc.(UCSY)</b></h4>
                                        <ul>
                                            <li>Cras dapibus dapibus</li>
                                            <li>Vivamus elementum semper nisi</li>
                                            <li>In enim justo, rhoncus ut</li>
                                        </ul>
                                        <a href="javascript:printpdf();">Click to Print the PDF</a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div><!-- /.col -->
                </div>
                <!-- fix for small devices only -->
                <div class="clearfix visible-sm-block"></div>

            </section><!-- /.row -->

            <div class="row">
                <div class="col-md-12">


                </div><!-- /.col -->
            </div><!-- /.row -->


        </section><!-- /.content -->

    </div><!-- /.content-wrapper -->
      <?php require_once '../worker_footer.php'; ?>
  </div>
</body>
</html>
