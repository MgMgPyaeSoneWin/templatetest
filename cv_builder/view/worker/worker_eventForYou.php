<!DOCTYPE html>
<html lang='en'>
  <head>

    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>CV Builder Registration</title>
    <?php require_once '../worker_header.php'; ?>
  </head>
<body class="skin-teal sidebar-mini">
  <div>
    <div class="wrapper">

      <?php require_once '../worker_navbar.php'; ?>
      <?php require_once '../worker_sidebar.php'; ?>


        <!-- Content Wrapper. Contains page content -->
        <section class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Dashboard
                    <small>Version 2.0</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li class="active">Dashboard</li>
                </ol>
            </section>

            <!-- Main content -->
            <section class="content">
                <!-- Info boxes -->
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <a href="#">
                            <div class="card cardPadding">
                                <div class="cardImage">
                                    <img src="../../public/img/applequater.jpg"/>
                                </div>
                                <div class="cardContent">
                                    <h4 class="h4-font-size center">
                                        One Short CV Release Event
                                    </h4>
                                    <dl class="dl-horizontal">
                                        <dt>Location</dt>
                                        <dd>No.38, Padather Street, Yangon</dd>
                                        <dt>Date / Time</dt>
                                        <dd>14/December/2016 - 1:00 pm to 3:00 pm</dd>
                                        <dt>Description</dt>
                                        <dd>Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</dd>
                                    </dl>
                                </div>
                            </div>
                        </a>
                    </div><!-- /.col -->
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <a href="#">
                            <div class="card cardPadding">
                                <div class="cardImage">
                                    <img src="../../public/img/applequater.jpg"/>
                                </div>
                                <div class="cardContent">
                                    <h4 class="h4-font-size center">
                                        One Short CV Release Event
                                    </h4>
                                    <dl class="dl-horizontal">
                                        <dt>Location</dt>
                                        <dd>No.38, Padather Street, Yangon</dd>
                                        <dt>Date / Time</dt>
                                        <dd>14/December/2016 - 1:00 pm to 3:00 pm</dd>
                                        <dt>Description</dt>
                                        <dd>Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</dd>
                                    </dl>
                                </div>
                            </div>
                        </a>
                    </div><!-- /.col -->
                </div>
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <a href="#">
                            <div class="card cardPadding">
                                <div class="cardImage">
                                    <img src="../../public/img/applequater.jpg"/>
                                </div>
                                <div class="cardContent">
                                    <h4 class="h4-font-size center">
                                        One Short CV Release Event
                                    </h4>
                                    <dl class="dl-horizontal">
                                        <dt>Location</dt>
                                        <dd>No.38, Padather Street, Yangon</dd>
                                        <dt>Date / Time</dt>
                                        <dd>14/December/2016 - 1:00 pm to 3:00 pm</dd>
                                        <dt>Description</dt>
                                        <dd>Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</dd>
                                    </dl>
                                </div>
                            </div>
                        </a>
                    </div><!-- /.col -->
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <a href="#">
                            <div class="card cardPadding">
                                <div class="cardImage">
                                    <img src="../../public/img/applequater.jpg"/>
                                </div>
                                <div class="cardContent">
                                    <h4 class="h4-font-size center">
                                        One Short CV Release Event
                                    </h4>
                                    <dl class="dl-horizontal">
                                        <dt>Location</dt>
                                        <dd>No.38, Padather Street, Yangon</dd>
                                        <dt>Date / Time</dt>
                                        <dd>14/December/2016 - 1:00 pm to 3:00 pm</dd>
                                        <dt>Description</dt>
                                        <dd>Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</dd>
                                    </dl>
                                </div>
                            </div>
                        </a>
                    </div><!-- /.col -->
                </div>
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <a href="#">
                            <div class="card cardPadding">
                                <div class="cardImage">
                                    <img src="../../public/img/applequater.jpg"/>
                                </div>
                                <div class="cardContent">
                                    <h4 class="h4-font-size center">
                                        One Short CV Release Event
                                    </h4>
                                    <dl class="dl-horizontal">
                                        <dt>Location</dt>
                                        <dd>No.38, Padather Street, Yangon</dd>
                                        <dt>Date / Time</dt>
                                        <dd>14/December/2016 - 1:00 pm to 3:00 pm</dd>
                                        <dt>Description</dt>
                                        <dd>Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</dd>
                                    </dl>
                                </div>
                            </div>
                        </a>
                    </div><!-- /.col -->
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <a href="#">
                            <div class="card cardPadding">
                                <div class="cardImage">
                                    <img src="../../public/img/applequater.jpg"/>
                                </div>
                                <div class="cardContent">
                                    <h4 class="h4-font-size center">
                                        One Short CV Release Event
                                    </h4>
                                    <dl class="dl-horizontal">
                                        <dt>Location</dt>
                                        <dd>No.38, Padather Street, Yangon</dd>
                                        <dt>Date / Time</dt>
                                        <dd>14/December/2016 - 1:00 pm to 3:00 pm</dd>
                                        <dt>Description</dt>
                                        <dd>Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</dd>
                                    </dl>
                                </div>
                            </div>
                        </a>
                    </div><!-- /.col -->
                </div>
                <!-- fix for small devices only -->
                <div class="clearfix visible-sm-block"></div>

            </section><!-- /.row -->

            <div class="row">
                <div class="col-md-12">


                </div><!-- /.col -->
            </div><!-- /.row -->


        </section><!-- /.content -->


    </div><!-- /.content-wrapper -->
      <?php require_once '../worker_footer.php'; ?>
  </div>
</body>
</html>
