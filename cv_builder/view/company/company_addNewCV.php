<!DOCTYPE html>
<html lang='en'>
<head>

    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>CV Builder Registration</title>
    <?php require_once '../company_header.php'; ?>
</head>
<body class="skin-teal sidebar-mini">
<div>
    <div class="wrapper">

        <?php require_once '../company_navbar.php'; ?>
        <?php require_once '../company_sidebar.php'; ?>


        <!-- Content Wrapper. Contains page content -->
        <section class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Add New CV
                    <small>Version 2.0</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li class="active">Add New CV</li>
                </ol>
            </section>

            <!-- Main content -->
            <section class="content">
                <!-- Info boxes -->
                <div class="row">
                    <div class="col-md-12 col-sm-6 col-xs-12">
                        <div class="card cardPadding">
                            <div class="cardContent">
                                <div class="cardheader">
                                    <h3 class="h4-font-size">
                                        Create your Jobs Post
                                    </h3>
                                </div>
                                <!--<h3 class="box-title">General Elements</h3>-->
                                <form role="form">
                                    <!-- text input -->
                                    <div class="form-group">
                                        <label>Job Title</label>
                                        <input type="text" class="form-control" placeholder="Enter ..."/>
                                    </div>
                                    <div class="form-group">
                                        <label>Job Description</label>
                                        <textarea class="form-control" rows="3" placeholder="Enter ..."></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Required Skill</label>
                                        <input type="text" class="form-control" placeholder="Enter ..."/>
                                    </div>
                                    <div class="form-group">
                                        <label>Department</label>
                                        <input type="text" class="form-control" placeholder="Enter ..."/>
                                    </div>
                                    <div class="form-group">
                                        <label>City</label>
                                        <input type="text" class="form-control" placeholder="Enter ..."/>
                                    </div>

                                    <div class="form-group">
                                        <label>Expired Date</label>
                                        <input type="text" class="form-control" placeholder="Enter ..."/>
                                    </div>

                                    <div class="form-group">
                                        <label>Content Type</label>
                                        <input type="text" class="form-control" placeholder="Enter ..."/>
                                    </div>


                                    <div class="form-group">
                                        <label>Degree</label>
                                        <input type="text" class="form-control" placeholder="Enter ..."/>
                                    </div>


                                    <div class="form-group">
                                        <label>Year Experiences</label>
                                        <input type="text" class="form-control" placeholder="Enter ..."/>
                                    </div>


                                    <div class="form-group">
                                        <label>Language Skill</label>
                                        <input type="text" class="form-control" placeholder="Enter ..."/>
                                    </div>

                                    <div class="box-footer">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                        <button type="submit" class="btn btn-default">Cancel</button>
                                    </div>

                                </form>

                            </div>
                        </div>
                    </div><!-- /.col -->
                </div>
                <!-- fix for small devices only -->
                <div class="clearfix visible-sm-block"></div>

            </section><!-- /.row -->

            <div class="row">
                <div class="col-md-12">


                </div><!-- /.col -->
            </div><!-- /.row -->


        </section><!-- /.content -->

        <?php require_once '../company_footer.php'; ?>
    </div>
</body>
</html>
