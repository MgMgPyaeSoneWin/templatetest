<!DOCTYPE html>
<html lang='en'>
<head>

    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>CV Builder Registration</title>
    <?php require_once '../company_header.php'; ?>
</head>
<body class="skin-teal sidebar-mini">
<div>
    <div class="wrapper">

        <?php require_once '../company_navbar.php'; ?>
        <?php require_once '../company_sidebar.php'; ?>

        <!-- Content Wrapper. Contains page content -->
        <section class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Create Event
                    <small>Version 1.0</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li class="active">Dashboard</li>
                </ol>
            </section>

            <!-- Main content -->
            <section class="content">
                <!-- Info boxes -->
                <div class="row">
                    <div class="col-md-12 col-sm-6 col-xs-12">
                        <div class="card cardPadding">
                            <div class="cardContent">
                                <div class="cardheader">
                                    <h3 class="h4-font-size">
                                        Make a recruitment now!!
                                    </h3>
                                </div>
                                <!--<h3 class="box-title">General Elements</h3>-->
                                <form role="form">
                                    <!-- text input -->
                                    <div class="form-group">
                                        <label>Event Photo</label>
                                        <form action="" method="post" enctype="multipart/form-data" id="js-upload-form">
                                            <div class="form-inline">
                                                <div class="form-group">
                                                    <input type="file" name="files[]" id="js-upload-files" multiple>
                                                </div>
                                                <button type="submit" class="btn btn-sm btn-primary" id="js-upload-submit">Upload files</button>
                                            </div>
                                        </form>

                                        <!-- Drop Zone -->
                                        <h4>Or drag and drop files below</h4>
                                        <div class="upload-drop-zone" id="drop-zone">
                                            Just drag and drop files here
                                        </div>

                                        <!-- Progress Bar -->
                                        <!--<div class="progress">-->
                                        <!--<div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">-->
                                        <!--<span class="sr-only">60% Complete</span>-->
                                        <!--</div>-->
                                        <!--</div>-->

                                        <!-- Upload Finished -->
                                        <div class="js-upload-finished">
                                            <h3>Processed files</h3>
                                            <div class="list-group">
                                                <a href="#" class="list-group-item list-group-item-success"><span class="badge alert-success pull-right">Success</span>image-01.jpg</a>
                                                <a href="#" class="list-group-item list-group-item-success"><span class="badge alert-success pull-right">Success</span>image-02.jpg</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Event Name</label>
                                        <input type="text" class="form-control" placeholder="Enter ..."/>
                                    </div>
                                    <div class="form-group">
                                        <label>Location</label>
                                        <input type="text" class="form-control" placeholder="Enter ..."/>
                                    </div>
                                    <!-- Date and time range -->
                                    <div class="form-group">
                                        <label>Date and time range:</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-clock-o"></i>
                                            </div>
                                            <input type="text" class="form-control" id="reservationtime"/>
                                        </div><!-- /.input group -->
                                    </div><!-- /.form group -->
                                    <div class="form-group">
                                        <label>Description</label>
                                        <textarea class="form-control" rows="3" placeholder="Enter ..."></textarea>
                                    </div>


                                    <div class="form-group">
                                        <label>Year Experiences</label>
                                        <input type="text" class="form-control" placeholder="Enter ..."/>
                                    </div>


                                    <div class="form-group">
                                        <label>Language Skill</label>
                                        <input type="text" class="form-control" placeholder="Enter ..."/>
                                    </div>

                                    <div class="box-footer">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                        <button type="submit" class="btn btn-default">Cancel</button>
                                    </div>

                                </form>

                            </div>
                        </div>
                    </div><!-- /.col -->
                </div>
                <!-- fix for small devices only -->
                <div class="clearfix visible-sm-block"></div>

            </section><!-- /.row -->

            <div class="row">
                <div class="col-md-12">
                </div><!-- /.col -->
            </div><!-- /.row -->

        </section><!-- /.content -->

        <?php require_once '../company_footer.php'; ?>
    </div>
</body>
</html>
