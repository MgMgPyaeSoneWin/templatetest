<!DOCTYPE html>
<html lang='en'>
<head>

    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>CV Builder Registration</title>
    <?php require_once '../company_header.php'; ?>
</head>
<body class="skin-teal sidebar-mini">
<div>
    <div class="wrapper">

        <?php require_once '../company_navbar.php'; ?>
        <?php require_once '../company_sidebar.php'; ?>

        <!-- Content Wrapper. Contains page content -->
        <section class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Dashboard
                    <small>Version 2.0</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li class="active">Dashboard</li>
                </ol>
            </section>

            <!-- Main content -->
            <section class="content">
                <!-- Info boxes -->
                <div class="row center">
                    <div class="col-md-6 col-md-offset-3 col-sm-6 col-xs-12">
                        <div class="card cardPadding">
                            <div class="punch"></div>
                            <div class="cardContent package-element">
                                <div class="cardheader">
                                    <h4 class="h4-font-size">
                                        Gold Package
                                    </h4>
                                </div>
                                <p class="disableText">
                                    By Admin / May 3rd,2014 / Design / 1 comment
                                </p>
                                <p class="cardContentFont">
                                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit........
                                </p>
                                <a class="btn btn-app">
                                    <span class="badge bg-teal">67</span>
                                    <i class="fa fa-inbox"></i> Orders
                                </a>
                            </div>
                        </div>
                    </div><!-- /.col -->
                </div>
                <div class="row center">

                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="card cardPadding">
                            <div class="punch"></div>
                            <div class="cardContent package-element">
                                <div class="cardheader">
                                    <h4 class="h4-font-size">
                                        Pletinum Package
                                    </h4>
                                </div>
                                <p class="disableText">
                                    By Admin / May 3rd,2014 / Design / 1 comment
                                </p>
                                <p class="cardContentFont">
                                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit........
                                </p>
                                <a class="btn btn-app">
                                    <span class="badge bg-teal">67</span>
                                    <i class="fa fa-inbox"></i> Orders
                                </a>
                            </div>
                        </div>
                    </div><!-- /.col -->
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="card cardPadding">
                            <div class="punch"></div>
                            <div class="cardContent package-element">
                                <div class="cardheader">
                                    <h4 class="h4-font-size">
                                        Pletinum Package
                                    </h4>
                                </div>
                                <p class="disableText">
                                    By Admin / May 3rd,2014 / Design / 1 comment
                                </p>
                                <p class="cardContentFont">
                                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit........
                                </p>
                                <a class="btn btn-app">
                                    <span class="badge bg-teal">67</span>
                                    <i class="fa fa-inbox"></i> Orders
                                </a>
                            </div>
                        </div>
                    </div><!-- /.col -->
                </div>
                <!-- fix for small devices only -->
                <div class="clearfix visible-sm-block"></div>

            </section><!-- /.row -->

            <div class="row">
                <div class="col-md-12">


                </div><!-- /.col -->
            </div><!-- /.row -->


        </section><!-- /.content -->

        <?php require_once '../company_footer.php'; ?>
    </div>
</body>
</html>
