<!DOCTYPE html>
<html lang='en'>
<head>

    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>CV Builder Registration</title>
    <?php require_once '../company_header.php'; ?>
</head>
<body class="skin-teal sidebar-mini">
<div>
    <div class="wrapper">

        <?php require_once '../company_navbar.php'; ?>
        <?php require_once '../company_sidebar.php'; ?>

        <!-- Content Wrapper. Contains page content -->
        <section class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Posted Jobs
                    <small>Company Panel</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li class="active">Posted Jobs</li>
                </ol>
            </section>

            <!-- Main content -->
            <section class="content">
                <!-- Info boxes -->
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <a href="cvDetailView.html">
                            <div class="card cardPadding">
                                <div class="cardContent image-background">
                                    <div class="user-header">
                                        <img src="../../img/user2-160x160.jpg" class="img-circle" alt="User Image">
                                        <p>
                                            Alexander Pierce - Web Developer
                                            <br/>
                                            <small>Member since Nov. 2012</small>
                                            <br/>
                                            <small>B.Csc Computer Sciences</small>
                                        </p>
                                    </div>
                                    <p class="left-and-right-algin">
                                        <i class="fa fa-heart fa-heart-o custom-heart"></i><i class="right-align">See
                                            more</i>
                                    </p>
                                </div>
                            </div>
                        </a>
                    </div><!-- /.col -->
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <a href="cvDetailView.html">
                            <div class="card cardPadding">
                                <div class="cardContent image-background">
                                    <div class="user-header">
                                        <img src="../../img/user2-160x160.jpg" class="img-circle" alt="User Image">
                                        <p>
                                            Alexander Pierce - Web Developer
                                            <br/>
                                            <small>Member since Nov. 2012</small>
                                            <br/>
                                            <small>B.Csc Computer Sciences</small>
                                        </p>
                                    </div>
                                    <p class="left-and-right-algin">
                                        <i class="fa fa-heart fa-heart-o custom-heart"></i><i class="right-align">See
                                            more</i>
                                    </p>
                                </div>
                            </div>
                        </a>
                    </div><!-- /.col -->
                </div>
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <a href="cvDetailView.html">
                            <div class="card cardPadding">
                                <div class="cardContent image-background">
                                    <div class="user-header">
                                        <img src="../../img/user2-160x160.jpg" class="img-circle" alt="User Image">
                                        <p>
                                            Alexander Pierce - Web Developer
                                            <br/>
                                            <small>Member since Nov. 2012</small>
                                            <br/>
                                            <small>B.Csc Computer Sciences</small>
                                        </p>
                                    </div>
                                    <p class="left-and-right-algin">
                                        <i class="fa fa-heart fa-heart-o custom-heart"></i><i class="right-align">See
                                            more</i>
                                    </p>
                                </div>
                            </div>
                        </a>
                    </div><!-- /.col -->
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <a href="cvDetailView.html">
                            <div class="card cardPadding">
                                <div class="cardContent image-background">
                                    <div class="user-header">
                                        <img src="../../img/user2-160x160.jpg" class="img-circle" alt="User Image">
                                        <p>
                                            Alexander Pierce - Web Developer
                                            <br/>
                                            <small>Member since Nov. 2012</small>
                                            <br/>
                                            <small>B.Csc Computer Sciences</small>
                                        </p>
                                    </div>
                                    <p class="left-and-right-algin">
                                        <i class="fa fa-heart fa-heart-o custom-heart"></i><i class="right-align">See
                                            more</i>
                                    </p>
                                </div>
                            </div>
                        </a>
                    </div><!-- /.col -->
                </div>
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <a href="cvDetailView.html">
                            <div class="card cardPadding">
                                <div class="cardContent image-background">
                                    <div class="user-header">
                                        <img src="../../img/user2-160x160.jpg" class="img-circle" alt="User Image">
                                        <p>
                                            Alexander Pierce - Web Developer
                                            <br/>
                                            <small>Member since Nov. 2012</small>
                                            <br/>
                                            <small>B.Csc Computer Sciences</small>
                                        </p>
                                    </div>
                                    <p class="left-and-right-algin">
                                        <i class="fa fa-heart fa-heart-o custom-heart"></i><i class="right-align">See
                                            more</i>
                                    </p>
                                </div>
                            </div>
                        </a>
                    </div><!-- /.col -->
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <a href="cvDetailView.html">
                            <div class="card cardPadding">
                                <div class="cardContent image-background">
                                    <div class="user-header">
                                        <img src="../../img/user2-160x160.jpg" class="img-circle" alt="User Image">
                                        <p>
                                            Alexander Pierce - Web Developer
                                            <br/>
                                            <small>Member since Nov. 2012</small>
                                            <br/>
                                            <small>B.Csc Computer Sciences</small>
                                        </p>
                                    </div>
                                    <p class="left-and-right-algin">
                                        <i class="fa fa-heart fa-heart-o custom-heart"></i><i class="right-align">See
                                            more</i>
                                    </p>
                                </div>
                            </div>
                        </a>
                    </div><!-- /.col -->
                </div>
                <!-- fix for small devices only -->
                <div class="clearfix visible-sm-block"></div>

            </section><!-- /.row -->

            <div class="row">
                <div class="col-md-12">


                </div><!-- /.col -->
            </div><!-- /.row -->


        </section><!-- /.content -->

        <?php require_once '../company_footer.php'; ?>
    </div>
</body>
</html>
