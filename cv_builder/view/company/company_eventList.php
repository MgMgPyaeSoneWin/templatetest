<!DOCTYPE html>
<html lang='en'>
<head>

    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>CV Builder Registration</title>
    <?php require_once '../company_header.php'; ?>
</head>
<body class="skin-teal sidebar-mini">
<div>
    <div class="wrapper">

        <?php require_once '../company_navbar.php'; ?>
        <?php require_once '../company_sidebar.php'; ?>

        <!-- Content Wrapper. Contains page content -->
        <section class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Dashboard
                    <small>Version 2.0</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li class="active">Event</li>
                </ol>
            </section>

            <!-- Main content -->
            <section class="content">
                <!-- Info boxes -->
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <a href="postedJobsForEvents.html">
                            <div class="card cardPadding">
                                <div class="cardImage">
                                    <img src="../../public/img/applequater.jpg"/>
                                </div>
                                <div class="cardContent">
                                    <h4 class="h4-font-size center">
                                        One Short CV Release Event
                                    </h4>
                                    <dl class="dl-horizontal">
                                        <dt>Location</dt>
                                        <dd>No.38, Padather Street, Yangon</dd>
                                        <dt>Date / Time</dt>
                                        <dd>14/December/2016 - 1:00 pm to 3:00 pm</dd>
                                        <dd>Donec id elit non mi porta gravida at eget metus.</dd>
                                        <dt>Description</dt>
                                        <dd>Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</dd>
                                    </dl>
                                </div>
                            </div>
                        </a>
                    </div><!-- /.col -->
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <a href="postedJobsForEvents.html">
                            <div class="card cardPadding cardDisable">
                                <div class="cardImage">
                                    <img src="../../public/img/applequater.jpg"/>
                                </div>
                                <div class="cardContent">
                                    <h4 class="h4-font-size center">
                                        One Short CV Release Event ( Passed )
                                    </h4>
                                    <dl class="dl-horizontal">
                                        <dt>Location</dt>
                                        <dd>No.38, Padather Street, Yangon</dd>
                                        <dt>Date / Time</dt>
                                        <dd>14/December/2016 - 1:00 pm to 3:00 pm</dd>
                                        <dd>Donec id elit non mi porta gravida at eget metus.</dd>
                                        <dt>Description</dt>
                                        <dd>Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</dd>
                                    </dl>
                                </div>
                            </div>
                        </a>
                    </div><!-- /.col -->
                </div>
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <a href="postedJobsForEvents.html">
                            <div class="card cardPadding">
                                <div class="cardImage">
                                    <img src="../../public/img/applequater.jpg"/>
                                </div>
                                <div class="cardContent">
                                    <h4 class="h4-font-size">
                                        Apply now to grab opporutnties
                                    </h4>
                                    <p class="disableText">
                                        By Admin / May 3rd,2014 / Design / 1 comment
                                    </p>
                                    <p class="cardContentFont">
                                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit.........
                                    </p>
                                    <p class="left-and-right-algin">
                                        <i class="fa fa-heart fa-heart-o custom-heart"></i><i class="right-align">See More</i>
                                    </p>
                                </div>
                            </div>
                        </a>
                    </div><!-- /.col -->
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <a href="postedJobsForEvents.html">
                            <div class="card cardPadding">
                                <div class="cardImage">
                                    <img src="../../public/img/applequater.jpg"/>
                                </div>
                                <div class="cardContent">
                                    <h4 class="h4-font-size">
                                        Apply now to grab opporutnties
                                    </h4>
                                    <p class="disableText">
                                        By Admin / May 3rd,2014 / Design / 1 comment
                                    </p>
                                    <p class="cardContentFont">
                                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit.........
                                    </p>
                                    <p class="left-and-right-algin">
                                        <i class="fa fa-heart fa-heart-o custom-heart"></i><i class="right-align">See More</i>
                                    </p>
                                </div>
                            </div>
                        </a>
                    </div><!-- /.col -->
                </div>
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <a href="postedJobsForEvents.html">
                            <div class="card cardPadding">
                                <div class="cardImage">
                                    <img src="../../public/img/applequater.jpg"/>
                                </div>
                                <div class="cardContent">
                                    <h4 class="h4-font-size">
                                        Apply now to grab opporutnties
                                    </h4>
                                    <p class="disableText">
                                        By Admin / May 3rd,2014 / Design / 1 comment
                                    </p>
                                    <p class="cardContentFont">
                                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit.........
                                    </p>
                                    <p class="left-and-right-algin">
                                        <i class="fa fa-heart fa-heart-o custom-heart"></i><i class="right-align">See More</i>
                                    </p>
                                </div>
                            </div>
                        </a>
                    </div><!-- /.col -->
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <a href="postedJobsForEvents.html">
                            <div class="card cardPadding">
                                <div class="cardImage">
                                    <img src="../../public/img/applequater.jpg"/>
                                </div>
                                <div class="cardContent">
                                    <h4 class="h4-font-size">
                                        Apply now to grab opporutnties
                                    </h4>
                                    <p class="disableText">
                                        By Admin / May 3rd,2014 / Design / 1 comment
                                    </p>
                                    <p class="cardContentFont">
                                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit.........
                                    </p>
                                    <p class="left-and-right-algin">
                                        <i class="fa fa-heart fa-heart-o custom-heart"></i><i class="right-align">See More</i>
                                    </p>
                                </div>
                            </div>
                        </a>
                    </div><!-- /.col -->
                </div>
                <!-- fix for small devices only -->
                <div class="clearfix visible-sm-block"></div>

            </section><!-- /.row -->

            <div class="row">
                <div class="col-md-12">


                </div><!-- /.col -->
            </div><!-- /.row -->


        </section><!-- /.content -->

        <?php require_once '../company_footer.php'; ?>
    </div>
</body>
</html>
