<!-- AdminLTE App -->
<script src="../../js/app.min.js" type="text/javascript"></script>

<footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> 2.0
        </div>
        <strong>Copyright &copy; 2014-2015 <a href="#">Pyae Sone</a>.</strong> All rights reserved.
</footer>
