<aside class="main-sidebar">
            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">
                <!-- Sidebar user panel -->
                <div class="user-panel">
                    <div class="pull-left image">
                        <img src="../../img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
                    </div>
                    <div class="pull-left info">
                        <p>Worker Name</p>

                        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                    </div>
                </div>
                <!-- search form -->

                <!-- <form action="#" method="get" class="sidebar-form">
                  <div class="input-group">
                    <input type="text" name="q" class="form-control" placeholder="Search..."/>
                    <span class="input-group-btn">
                      <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                    </span>
                  </div>
                </form> -->

                <!-- /.search form -->
                <!-- sidebar menu: : style can be found in sidebar.less -->
                <ul class="sidebar-menu">
                    <li class="header">Worker Panel</li>
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <li class="treeview">
                        <a href="worker_eventForYou.php">
                            <i class="fa fa-dashboard"></i> <span>Event For You</span> <i
                                class="fa fa-angle-right pull-right"></i>
                        </a>
                        <!-- <ul class="treeview-menu">
                          <li><a href="index.html"><i class="fa fa-circle-o"></i> Dashboard v1</a></li>
                          <li class="active"><a href="index2.html"><i class="fa fa-circle-o"></i> Dashboard v2</a></li>
                        </ul> -->
                    </li>
                    <li class="treeview">
                        <a href="worker_yourCVDetail.php">
                            <i class="fa fa-language"></i>
                            <span>Your CV Detail</span><i
                                class="fa fa-angle-right pull-right"></i>
                        </a>
                    </li>
                    <li class="treeview">
                        <a href="worker_viewAppliedJob.php">
                            <i class="fa fa-files-o"></i>
                            <span>Viewed Applied Jobs</span>
                            <i class="fa fa-angle-right pull-right"></i>
                        </a>
                    </li>
                    <li>
                        <a href="worker_searchJobs.php">
                            <i class="fa fa-th"></i> <span>Search Jobs</span>
                            <i class="fa fa-angle-right pull-right"></i>
                        </a>
                    </li>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-edit"></i> <span>Account Setting</span>
                            <i class="fa fa-angle-right pull-right"></i>
                        </a>
                    </li>
                </ul>

                </ul>
            </section>
            <!-- /.sidebar -->
        </aside>
