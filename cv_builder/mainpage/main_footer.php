<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                &copy; 2015 <a target="_blank" href="http://bootstraptaste.com/"
                               title="Free Twitter Bootstrap WordPress Themes and HTML templates"
                               style="color: #ffffff;">One Short CV</a>. All Rights Reserved.
            </div>
        </div>
    </div>
</footer><!--/#footer-->
