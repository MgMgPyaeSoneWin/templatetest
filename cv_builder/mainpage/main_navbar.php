<header>
    <nav class="navbar navbar-default fixed">
        <div class="container">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">1 Short CV</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav" style="background-color: #FFC107;">
                        <li class="activePS" style="background-color: #FFC107;"><a href="#">Find Jobs<span
                                class="sr-only">(current)</span></a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="#">About & Contact</a></li>
                        <li><a href="login.html">Login</a></li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </div>
    </nav>

</header><!--/header-->

<div class="containerPS">
    <div class="container">
        <div class="jumbotron" style="background-color: #009688; color:#FFFFFF;">
            <h1 class="center">Short CV Froms in one minute.</h1>
            <p class="center">
                <a class="btn btn-primary btn-lg" href="view/worker/worker_register.php" role="button">Register as Company</a>
                <a class="btn btn-info btn-lg" href="view/worker/worker_register.php" role="button">Register as Job Seeker</a>
            </p>
        </div>
    </div>
</div>
