<!DOCTYPE html>
<html lang='en'>
  <head>

    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>CV Builder</title>
    <?php require_once 'mainpage/main_header.php'; ?>
  </head>
  <body>
    <?php require_once 'mainpage/main_navbar.php'; ?>
    <?php require_once 'mainpage/main_content.php'; ?>
    <?php require_once 'mainpage/main_footer.php'; ?>
  </body>
</html>
